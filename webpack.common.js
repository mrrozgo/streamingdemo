const path = require('path');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    entry: ['babel-polyfill', './src/index.js'],
    mode: 'development',
    output: {
        path: path.resolve(__dirname, 'dist/'),
        publicPath: '/',
        filename: '[name].[contenthash].bundle.js',
    },
    resolve: {
        extensions: ['*', '.mjs', '.js', '.jsx'],
    },
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                loader: 'babel-loader',
                options: {
                    presets: ['@babel/env', '@babel/preset-react'],
                },
            },
            {
                test: /\.css/,
                use: ['style-loader', 'css-loader'],
            },
        ],
    },
    plugins: [
        new CleanWebpackPlugin(),
        new HtmlWebpackPlugin({
            title: 'StreamDemo',
            template: 'template.html',
            filename: 'index.html',
        }),
    ],
};
