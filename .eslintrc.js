module.exports = {
    extends: 'airbnb',
    env: {
        browser: true,
        node: true
    },
    rules: {
        indent: ['error', 4],
        'no-underscore-dangle': ['error', { 'allowAfterThis': true }],
        'react/jsx-indent': ['error', 4],
        'react/jsx-indent-props': ['error', 4],
        'jsx-quotes': ['error', 'prefer-single'],
        'arrow-parens': [2, 'as-needed', { 'requireForBlockBody': true }],
        'react/jsx-props-no-spreading': 'off',
        'react/jsx-fragments': 'off',
        'react/forbid-prop-types': 'off',
    }
};