const merge = require('webpack-merge');
const common = require('./webpack.common.js');

module.exports = merge(common, {
    mode: 'development',
    devtool: 'inline-source-map',
    devServer: {
        compress: true,
        // contentBase: path.join(__dirname, '/dist/'),
        port: 3000,
        headers: {
            'Access-Control-Allow-Origin': '*',
        },
    },
});
