/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const onCreateChatMessage = `subscription OnCreateChatMessage($streamroom: String!) {
  onCreateChatMessage(streamroom: $streamroom) {
    id
    userid
    content
    timestamp
    streamroom
  }
}
`;
