/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const listStreamRooms = `query ListStreamRooms($userid: String!) {
  listStreamRooms(userid: $userid) {
    id
    name
    value
    userid
    timestamp
  }
}
`;
export const listChatMessages = `query ListChatMessages($streamroom: String!, $limit: Int!, $nextToken: String) {
  listChatMessages(
    streamroom: $streamroom
    limit: $limit
    nextToken: $nextToken
  ) {
    items {
      id
      userid
      content
      timestamp
      streamroom
    }
    nextToken
  }
}
`;
export const getChatMessage = `query GetChatMessage($id: ID!) {
  getChatMessage(id: $id) {
    id
    userid
    content
    timestamp
    streamroom
  }
}
`;
