/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const addStreamRoom = `mutation AddStreamRoom(
  $name: String!
  $value: String!
  $userid: String!
  $timestamp: AWSTimestamp!
) {
  addStreamRoom(
    name: $name
    value: $value
    userid: $userid
    timestamp: $timestamp
  ) {
    id
    name
    value
    userid
    timestamp
  }
}
`;
export const createChatMessage = `mutation CreateChatMessage(
  $userid: String!
  $content: String!
  $timestamp: AWSTimestamp!
  $streamroom: String!
) {
  createChatMessage(
    userid: $userid
    content: $content
    timestamp: $timestamp
    streamroom: $streamroom
  ) {
    id
    userid
    content
    timestamp
    streamroom
  }
}
`;
