// a pseudo locale

const LC_DEFAULT = 'EN_US';

const pseudoLc = {
    EN_US: {
        btn_addStream_default: 'Add stream',
        btn_addStream_fetching: 'Fetching...',
        btn_addStream_adding: 'Submitting...',
        sel_stream_label: 'Select stream to display',
        input_streampath_label: 'Stream path',
        input_streamname_label: 'Stream name',
        sel_videoqual_label: 'Select quality',
        status_chat_sending: 'Sending message...',
        status_chat_fetching: 'Fetching ald messages...',
        status_chat_not_connected: 'Not connected!',
        status_chat_empty_header: 'The chat is empty!',
        status_chat_empty_content: 'You can leave a first message here.',
        input_chatmsg_add_label: 'User:',
        btn_addmsg_sending: 'Sending',
        btn_addmsg_default: 'Send',
    },
};

const getLcString = (id, lcId = LC_DEFAULT) => {
    const usedLc = pseudoLc[lcId] || pseudoLc[LC_DEFAULT];
    return usedLc[id] || id;
};

export default getLcString;
