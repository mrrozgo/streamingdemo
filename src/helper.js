// {Number} -> {Promise}
const delayResolve = ms => new Promise(resolve => setTimeout(() => resolve(), ms));

// {String, String} -> {Array}
const getDefaultStreamRooms = (userid, timestamp) => [{
    id: '1-1-1-1',
    value: 'https://test-streams.mux.dev/x36xhzz/x36xhzz.m3u8',
    name: 'stream1',
    userid,
    timestamp,
}, {
    id: '2-2-2-2',
    value: 'https://test-streams.mux.dev/test_001/stream.m3u8',
    name: 'stream2',
    userid,
    timestamp,
}, {
    id: '3-3-3-3',
    value: 'invalid',
    name: 'invalid stream (test)',
    userid,
    timestamp,
}];

export { delayResolve, getDefaultStreamRooms };
