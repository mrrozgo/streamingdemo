export default theme => ({
    appContainer: {
        fontFamily: theme.appFont,
        boxSizing: 'border-box',
        maxWidth: theme.appWidth,
        marginLeft: 'auto',
        marginRight: 'auto',
    },
    selectorFormContainer: {
        boxSizing: 'border-box',
        display: 'block',
        width: '100%',
        height: 'auto',
        marginBottom: theme.getSpacer('sm'),
    },
    chatAndPlayerContainer: {
        boxSizing: 'border-box',
    },
    playerContainer: {
        boxSizing: 'border-box',
    },
    chatContainer: {
        [theme.breakpoints.down('sm')]: {
            height: '480px',
        },
        [theme.breakpoints.up('md')]: {
            height: '600px',
        },
        boxSizing: 'border-box',
    },
});
