import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Fingerprint from 'fingerprintjs';
import { ThemeProvider } from '@material-ui/core/styles';
import { withStyles } from '@material-ui/styles';
import { Grid } from '@material-ui/core';

import StreamPlayer from './components/StreamPlayer/index';
import StreamChat from './components/StreamChat/StreamChat';
import StreamSelector from './components/StreamSelector/index';

import appTheme from './theme';
import appLayout from './appLayout';

class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            streamroom: '', // for test: 's1'
            streamState: -1,
            userId: new Fingerprint().get().toString(),
        };

        // far future: if api gateway is needed
        // this.ws1 = new WebSocket('wss://buml1puno7.execute-api.eu-central-1.amazonaws.com/stage1/');

        this.onSelectStream = this.onSelectStream.bind(this);
        this.onUpdPlayerState = this.onUpdPlayerState.bind(this);
    }

    onSelectStream(streamroom) {
        this.setState({ streamroom });
    }

    onUpdPlayerState(state) {
        this.setState({
            streamState: state,
        });
    }

    render() {
        const { state: { streamroom, userId, streamState }, props: { classes } } = this;

        return (
            <Grid container spacing={1} className={classes.appContainer}>
                <Grid item xs={12} className={classes.selectorFormContainer}>
                    <StreamSelector
                        userid={userId}
                        cbSelectStream={this.onSelectStream}
                        isDisabled={streamState === 0}
                    />
                </Grid>
                <Grid container item xs={12} spacing={1} className={classes.chatAndPlayerContainer}>
                    <Grid item xs={12} sm={6} md={8} className={classes.playerContainer}>
                        <StreamPlayer
                            cbUpdateState={this.onUpdPlayerState}
                            streamroom={streamroom}
                        />
                    </Grid>
                    <Grid item xs={12} sm={6} md={4} className={classes.chatContainer}>
                        <StreamChat
                            streamroom={streamroom}
                            userid={userId}
                            streamState={streamState}
                        />
                    </Grid>
                </Grid>
            </Grid>
        );
    }
}

App.propTypes = {
    classes: PropTypes.object.isRequired,
};

const StyledApp = withStyles(appLayout)(App);

export default () => (
    <ThemeProvider theme={appTheme}>
        <StyledApp />
    </ThemeProvider>
);
