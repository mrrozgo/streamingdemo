import { createMuiTheme } from '@material-ui/core/styles';

const theme = createMuiTheme({
    typography: {
        subtitle1: {
            fontSize: 16,
        },
        subtitle2: {
            fontSize: 14,
            lineHeight: '12px',
            letterSpacing: 'normal',
            fontWeight: 300,
        },
    },
    appFont: 'Arial, Helvetica, sans-serif',
    appWidth: '1280px',
    appHeight: '640px',
    bgGrey_1: '#f0f0f0',
    bgGrey_2: '#f9f9f9',
    border_1: '1px solid #a9a9a9',
    txtWhite_1: '#ffffff',
    fcMinWidth: '240px',
    shadow_1: '0px 0px 5px 2px rgba(160,160,160,1)',
    bRadius_1: '3px',
    bRadius_2: '6px',
    getSpacer: (val) => {
        switch (val) {
        case 'xs': return '3px';
        case 'sm': return '6px';
        case 'md': return '9px';
        case 'xl': return '12px';
        case 'xxl': return '18px';
        default: return '6px';
        }
    },
    wordBreaker: {
        wordBreak: 'break-word',
        wordWrap: 'break-word',
    },
    selectEntry: {
        fontSize: '14px',
        fontWeight: '300',
    },
});

export default theme;
