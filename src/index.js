import React from 'react';
import ReactDOM from 'react-dom';
import Amplify from 'aws-amplify';

import appsyncCfg from './aws-exports';

import App from './App';

Amplify.configure(appsyncCfg);

ReactDOM.render(React.createElement(App), document.getElementById('approot'));
