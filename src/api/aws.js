// api-layer implementation

import { API, graphqlOperation } from 'aws-amplify';

import { listChatMessages, listStreamRooms } from '../graphql/queries';
import { createChatMessage, addStreamRoom } from '../graphql/mutations';
import { onCreateChatMessage } from '../graphql/subscriptions';
import { delayResolve, getDefaultStreamRooms } from '../helper';

// {String, Number, String} -> {Promise}
async function addChatMessage(streamroom, userid, content) {
    await delayResolve(1500); // !FOR TESTING PURPOSE

    const result = await API.graphql(graphqlOperation(createChatMessage, {
        streamroom,
        userid,
        content,
        timestamp: Date.now(),
    }));

    return result.data.createChatMessage;
}

// {String, Number, String} -> {Promise}
async function getChatMessages(streamroom, limit, nextToken) {
    const result = await API.graphql(graphqlOperation(listChatMessages, {
        streamroom, limit, nextToken,
    }));

    return result.data.listChatMessages;
}

// {String, Function} -> {}
function subOnAddMsg(streamroom, onSuccess) {
    return API.graphql(graphqlOperation(onCreateChatMessage, {
        streamroom,
    })).subscribe({
        next: result => onSuccess(result.value.data.onCreateChatMessage),
    });
}

async function getStreamsList(userid) {
    await delayResolve(1500); // !FOR TESTING PURPOSE

    const result = await API.graphql(graphqlOperation(listStreamRooms, {
        userid,
    }));

    return getDefaultStreamRooms(userid, Date.now()).concat(result.data.listStreamRooms);
}

async function addStreamToList(streamName, streamPath, userid) {
    await delayResolve(1500); // !FOR TESTING PURPOSE

    const result = await API.graphql(graphqlOperation(addStreamRoom, {
        name: streamName,
        value: streamPath,
        userid,
        timestamp: Date.now(),
    }));

    return result.data.addStreamRoom;
}

export {
    addChatMessage,
    getChatMessages,
    subOnAddMsg,
    getStreamsList,
    addStreamToList,
};
