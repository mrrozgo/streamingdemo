import React, { PureComponent, createRef } from 'react';
import PropTypes from 'prop-types';
import Hls from 'hls.js';
import { withStyles } from '@material-ui/styles';
import {
    FormControl, InputLabel, Select, MenuItem,
} from '@material-ui/core';

import getLcString from '../../locale';
import {
    STREAM_STATE_FETCHING, STREAM_STATE_SUCCESS, STREAM_STATE_ERR,
} from '../../constants';

import styles from './styles';

class StreamPlayer extends PureComponent {
    constructor(props) {
        super(props);
        this._videoNodeRef = createRef();

        this.state = {
            // streamsInfoFetching: false,
            switchingQLvl: false,
            // isReady: false,
            statesLog: [],
            qLevels: [],
            currentQLevel: '',
        };

        this.onPlayClick = this.onPlayClick.bind(this);
        // this.onVideoEvent = this.onVideoEvent.bind(this);
        this.onQualityChange = this.onQualityChange.bind(this);

        this.onStreamErr = this.onStreamErr.bind(this);
        this.onMediaAttached = this.onMediaAttached.bind(this);
        this.onManifestLoading = this.onManifestLoading.bind(this);
        this.onManifestParsed = this.onManifestParsed.bind(this);
        this.onLevelSwitching = this.onLevelSwitching.bind(this);
        this.onLevelSwitched = this.onLevelSwitched.bind(this);
    }

    componentDidMount() {
        const { props: { streamroom } } = this;

        if (Hls.isSupported() && streamroom.length > 0) {
            this._hlsInst = new Hls();

            this.initHandlers();
            this._hlsInst.attachMedia(this._videoNodeRef.current);
        }
    }

    componentDidUpdate(prevProps) {
        const { props: { streamroom } } = this;

        if (Hls.isSupported() && prevProps.streamroom !== streamroom && streamroom.length > 0) {
            if (this._hlsInst) {
                this._hlsInst.detachMedia();
                this._hlsInst.attachMedia(this._videoNodeRef.current);
            } else {
                this._hlsInst = new Hls();

                this.initHandlers();
                this._hlsInst.attachMedia(this._videoNodeRef.current);
            }
        }
    }

    componentWillUnmount() {
        this.releaseHandlers();
    }

    onPlayClick() {
        this._videoNodeRef.current.play();
    }

    // !FAR FUTURE (if required)
    // onVideoEvent(e) {
    //     switch(e.type) {
    //         case 'seeking':
    //             break;
    //         case 'seeked':
    //             break
    //         case 'play':
    //             break;
    //         case 'playing':
    //             break;
    //         default:
    //             // console.log('default'); // !DEBUG
    //     }
    // }

    onQualityChange(e) {
        this._hlsInst.currentLevel = parseInt(e.target.value, 10);
        // console.log('onQualityChange', e.target.value, this._hlsInst.currentLevel); // !DEBUG
    }

    onStreamErr(e, data) {
        // console.info('Hls.Events.ERROR', data); // !DEBUG
        const { cbUpdateState } = this.props;

        if (data.fatal) {
            cbUpdateState(STREAM_STATE_ERR, data.type);

            switch (data.type) {
            case Hls.ErrorTypes.NETWORK_ERROR:
                if (data.details !== 'manifestLoadError') {
                    this._hlsInst.startLoad();
                } else {
                    this.setState({
                        qLevels: [],
                    }, () => {
                        this.releaseHandlers();
                        this.releaseContext();
                    });
                }

                break;
            case Hls.ErrorTypes.MEDIA_ERROR:
                cbUpdateState(STREAM_STATE_FETCHING, data.type);
                this._hlsInst.recoverMediaError();

                break;
            default:
                this.setState({
                    qLevels: [],
                }, () => {
                    this.releaseHandlers();
                    this.releaseContext();
                });

                break;
            }
        }
    }

    onLevelSwitching(e, data) {
        // console.info('Hls.Events.LEVEL_SWITCHING', e, data); // !DEBUG
        this.setState({
            switchingQLvl: true,
        });
    }

    onLevelSwitched(e, data) {
        // console.info('Hls.Events.LEVEL_SWITCHED', e, data); // !DEBUG
        this.setState({
            currentQLevel: data.level,
            switchingQLvl: false,
        });
    }

    onMediaAttached(e, data) {
        const { streamroom } = this.props;
        // console.info('Hls.Events.MEDIA_ATTACHED', e, data); // !DEBUG
        this._hlsInst.loadSource(streamroom);
    }

    onMediaDetached(e, data) {
        // console.info('Hls.Events.MEDIA_DETACHED', e, data); // !DEBUG
    }

    onManifestLoading(e, data) {
        const { cbUpdateState } = this.props;
        // console.info('Hls.Events.MANIFEST_LOADING', e, data); // !DEBUG
        cbUpdateState(STREAM_STATE_FETCHING);
    }

    onManifestParsed(e, data) {
        const { cbUpdateState } = this.props;
        // console.info('Hls.Events.MANIFEST_PARSED', e, data); // !DEBUG

        this.setState({
            qLevels: data.levels,
            currentQLevel: data.firstLevel,
        }, () => {
            cbUpdateState(STREAM_STATE_SUCCESS);
        });
    }

    releaseContext() {
        if (this._hlsInst) {
            this._hlsInst.destroy();
            this._hlsInst = null;
        }
    }

    initHandlers() {
        if (this._hlsInst) {
            this._hlsInst.on(Hls.Events.MEDIA_ATTACHED, this.onMediaAttached);
            this._hlsInst.on(Hls.Events.MANIFEST_LOADING, this.onManifestLoading);
            this._hlsInst.on(Hls.Events.MANIFEST_PARSED, this.onManifestParsed);
            this._hlsInst.on(Hls.Events.LEVEL_SWITCHING, this.onLevelSwitching);
            this._hlsInst.on(Hls.Events.LEVEL_SWITCHED, this.onLevelSwitched);
            this._hlsInst.on(Hls.Events.ERROR, this.onStreamErr);
        }
    }

    releaseHandlers() {
        if (this._hlsInst) {
            this._hlsInst.off(Hls.Events.MEDIA_ATTACHED, this.onMediaAttached);
            this._hlsInst.off(Hls.Events.MANIFEST_LOADING, this.onManifestLoading);
            this._hlsInst.off(Hls.Events.MANIFEST_PARSED, this.onManifestParsed);
            this._hlsInst.off(Hls.Events.LEVEL_SWITCHING, this.onLevelSwitching);
            this._hlsInst.off(Hls.Events.LEVEL_SWITCHED, this.onLevelSwitched);
            this._hlsInst.off(Hls.Events.ERROR, this.onStreamErr);
        }
    }

    render() {
        const {
            onVideoEvent, _videoNodeRef,
            state: {
                statesLog, switchingQLvl, currentQLevel, qLevels,
            },
            props: { classes },
        } = this;

        return (
            <div className={classes.streamPlayer}>
                <form className={classes.controlsContainer}>
                    <FormControl
                        className={classes.formControl}
                        disabled={switchingQLvl || qLevels.length === 0}
                    >
                        <InputLabel id='labelQSelect'>
                            {getLcString('sel_videoqual_label')}
                        </InputLabel>
                        <Select
                            value={currentQLevel}
                            disabled={switchingQLvl || qLevels.length === 0}
                            labelId='labelQSelect'
                            id='inputQSelect'
                            onChange={this.onQualityChange}
                        >
                            {qLevels.map((entry, ind) => (
                                <MenuItem className={classes.selectEntry} value={ind} key={`key_${entry.bitrate}`}>
                                    {entry.name || `${entry.width}x${entry.height} (${entry.bitrate})`}
                                </MenuItem>
                            ))}
                        </Select>
                    </FormControl>
                </form>
                <div className={classes.videoContainer}>
                    <video
                        controls
                        className={classes.videoPlayer}
                        onSeeked={onVideoEvent}
                        onSeeking={onVideoEvent}
                        onPause={onVideoEvent}
                        onPlay={onVideoEvent}
                        onLoadedMetadata={onVideoEvent}
                        onLoadedData={onVideoEvent}
                        onDurationChange={onVideoEvent}
                        ref={_videoNodeRef}
                        track=''
                    >
                        <track default kind='captions' label='labelId1' />
                    </video>
                </div>
            </div>
        );
    }
}

StreamPlayer.propTypes = {
    streamroom: PropTypes.string.isRequired,
    classes: PropTypes.object.isRequired,
    cbUpdateState: PropTypes.func.isRequired,
};

export default withStyles(styles)(StreamPlayer);
