const styles = theme => ({
    streamPlayer: {
        overflow: 'hidden',
        display: 'block',
        boxSizing: 'border-box',
        height: '100%',
        width: '100%',
    },
    controlsContainer: {
        boxSizing: 'border-box',
        borderRadius: '6px',
        width: '100%',
        padding: `0 ${theme.getSpacer('xl')}`,
        marginBottom: theme.getSpacer('xl'),
        backgroundColor: theme.bgGrey_1,
    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: theme.fcMinWidth,
    },
    videoContainer: {
        maxWidth: '100%',
        boxSizing: 'border-box',
    },
    videoPlayer: {
        height: 'auto',
        width: '100%',
    },
    selectEntry: {
        ...theme.selectEntry,
    },
    // videoReport: {
    //     padding: '6px',
    //     width: '350px',
    //     display: 'block',
    //     overflowY: 'scroll',
    //     overflowX: 'scroll',
    //     border: '1px solid black',
    // },
});

export default styles;
