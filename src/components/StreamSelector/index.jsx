/* eslint-disable max-classes-per-file */

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/styles';
import {
    FormControl, InputLabel, Select, MenuItem, TextField, Button,
} from '@material-ui/core';

import getLcString from '../../locale';

import { getStreamsList, addStreamToList } from '../../api/aws';
import styles from './styles';

class StreamSelector extends PureComponent {
    constructor(props) {
        super(props);

        this.state = {
            listFetching: false,
            addStreamFetching: false,
            // listErr: null,
            streamSources: [],
            streamroom: '',
            newStreamName: '',
            newStreamPath: '',
        };

        this.onChangeSelected = this.onChangeSelected.bind(this);
        this.onChangeNewStream = this.onChangeNewStream.bind(this);
        this.onSubmitNewStream = this.onSubmitNewStream.bind(this);
    }

    componentDidMount() {
        const { userid } = this.props;

        this.setState({
            listFetching: true,
            // listErr: null,
        }, () => getStreamsList(userid)
            .then((result) => {
                this.setState({
                    streamSources: result,
                    listFetching: false,
                });
            })
            .catch((err) => {
                this.setState({
                    listFetching: false,
                    // listErr: err,
                });
            }));
    }

    onChangeSelected(e) {
        const { cbSelectStream } = this.props;

        this.setState({
            streamroom: e.target.value,
        });

        cbSelectStream(e.target.value);
    }

    onChangeNewStream(e) {
        this.setState({
            [e.target.name]: e.target.value,
        });
    }

    onSubmitNewStream(e) {
        e.preventDefault();
        const { state: { newStreamName, newStreamPath, streamSources }, props: { userid } } = this;

        if (this.isAvailToSubmit()) {
            this.setState({
                addStreamFetching: true,
            }, () => addStreamToList(newStreamName, newStreamPath, userid)
                .then((result) => {
                    this.setState({
                        newStreamName: '',
                        newStreamPath: '',
                        addStreamFetching: false,
                        streamSources: streamSources.concat(result),
                    });
                }));
        }
    }

    getSubmitButonLabel() {
        const { listFetching, addStreamFetching } = this.state;

        if (listFetching) {
            return getLcString('btn_addStream_fetching');
        }

        if (addStreamFetching) {
            return getLcString('btn_addStream_adding');
        }

        return getLcString('btn_addStream_default');
    }

    isAvailToSubmit() {
        const {
            addStreamFetching, listFetching, newStreamName, newStreamPath, streamSources,
        } = this.state;

        if (
            !addStreamFetching
            && !listFetching
            && newStreamName.length !== 0
            && newStreamPath.length !== 0
            && !streamSources.find(entry => entry.value === newStreamPath)
        ) {
            return true;
        }

        return false;
    }

    render() {
        const {
            state: {
                streamSources, streamroom, listFetching, addStreamFetching,
                newStreamName, newStreamPath,
            },
            props: { classes, isDisabled },
        } = this;

        const isUiDisabled = listFetching || addStreamFetching || isDisabled;
        const isAvailToSubmit = this.isAvailToSubmit();

        return (
            <div className={classes.componentRoot}>
                <form
                    noValidate
                    autoComplete='off'
                >
                    <FormControl
                        disabled={isUiDisabled}
                        className={classes.formControl}
                    >
                        <InputLabel id='labelStreamSelect'>
                            {getLcString('sel_stream_label')}
                        </InputLabel>
                        <Select
                            value={streamroom}
                            labelId='labelStreamSelect'
                            id='inputStreamSelect'
                            onChange={this.onChangeSelected}
                            classes={{ select: classes.select }}
                        >
                            {streamSources.map(({ id, value, name }) => (
                                <MenuItem className={classes.selectEntry} key={`key_${id}`} value={value}>
                                    {name}
                                </MenuItem>
                            ))}
                        </Select>
                    </FormControl>
                </form>
                <form
                    onSubmit={this.onSubmitNewStream}
                    noValidate
                    autoComplete='off'
                >
                    <FormControl className={classes.formControl}>
                        <TextField
                            name='newStreamPath'
                            value={newStreamPath}
                            onChange={this.onChangeNewStream}
                            disabled={isUiDisabled}
                            required
                            label={getLcString('input_streampath_label')}
                        />
                    </FormControl>
                    <FormControl className={classes.formControl}>
                        <TextField
                            name='newStreamName'
                            value={newStreamName}
                            onChange={this.onChangeNewStream}
                            disabled={isUiDisabled}
                            required
                            label={getLcString('input_streamname_label')}
                        />
                    </FormControl>
                    <FormControl
                        disabled={listFetching || addStreamFetching}
                        className={classes.formControl}
                    >
                        <Button
                            disabled={!isAvailToSubmit}
                            type='submit'
                            variant='contained'
                            color='primary'
                        >
                            { this.getSubmitButonLabel() }
                        </Button>
                    </FormControl>
                </form>
            </div>
        );
    }
}

StreamSelector.propTypes = {
    userid: PropTypes.string.isRequired,
    cbSelectStream: PropTypes.func.isRequired,
    classes: PropTypes.object.isRequired,
    isDisabled: PropTypes.bool.isRequired,
};

export default withStyles(styles)(StreamSelector);

/* eslint-enable max-classes-per-file */
