const styles = theme => ({
    formControl: {
        margin: theme.spacing(1),
        minWidth: theme.fcMinWidth,
    },
    selectEmpty: {
        marginTop: theme.spacing(2),
    },
    select: {
        paddingLeft: theme.getSpacer('sm'),
    },
    selectEntry: {
        ...theme.selectEntry,
    },
    componentRoot: {
        boxSizing: 'border-box',
        padding: theme.getSpacer('sm'),
        borderRadius: '6px',
        backgroundColor: theme.bgGrey_1,
    },
});

export default styles;
