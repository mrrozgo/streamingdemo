/* eslint-disable max-classes-per-file */

import React, { Fragment, PureComponent } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/styles';

import {
    Avatar, Card, CardHeader, Typography, Grid,
} from '@material-ui/core';

import styles from './styles';

const getMsgDateHelper = (timestamp) => {
    const msgDate = new Date(timestamp).toISOString();
    const dateFormatted = `${msgDate.substr(0, 10)} ${msgDate.substr(11, 8)}`;

    return dateFormatted;
};

class ListMessage extends PureComponent {
    render() {
        const {
            timestamp, userid, content, idRoot, classes,
        } = this.props;
        const dateFormatted = getMsgDateHelper(timestamp);
        const isOwnMsg = idRoot === userid;

        return (
            <Grid
                wrap='nowrap'
                container
                direction={isOwnMsg ? 'row-reverse' : 'row'}
                className={classes.entryContainer}
                alignItems='flex-end'
            >
                <Grid item>
                    <Avatar aria-label='user-ava'>
                        {userid.length > 1 ? userid[0] + userid[1] : userid[0]}
                    </Avatar>
                </Grid>
                <Grid item>
                    <Card className={classes.chatMsg}>
                        <CardHeader
                            className={classes.msgHeader}
                            title={(
                                <Fragment>
                                    <Typography
                                        color='primary'
                                        variant='subtitle1'
                                        component='span'
                                    >
                                        {isOwnMsg ? 'Me: ' : `${userid}: `}
                                    </Typography>
                                    <Typography
                                        color='textPrimary'
                                        variant='subtitle2'
                                        component='span'
                                    >
                                        {content}
                                    </Typography>
                                </Fragment>
                            )}
                            subheader={(
                                <Typography
                                    component='i'
                                    color='textSecondary'
                                >
                                    {dateFormatted}
                                </Typography>
                            )}
                        />
                    </Card>
                </Grid>
            </Grid>
        );
    }
}

ListMessage.propTypes = {
    timestamp: PropTypes.number.isRequired,
    userid: PropTypes.string.isRequired,
    idRoot: PropTypes.string.isRequired,
    content: PropTypes.string.isRequired,
    classes: PropTypes.object.isRequired,
};

const ListMessageStyled = withStyles(styles)(ListMessage);

class ChatMessagesList extends PureComponent {
    constructor(props) {
        super(props);

        this.mesagesListRef = React.createRef();
    }

    componentDidUpdate(prevProps) {
        const { props: { dataEntries, keepScrollOnUpdate }, mesagesListRef } = this;

        if (dataEntries !== prevProps.dataEntries) {
            if (keepScrollOnUpdate) {
                mesagesListRef.current.scrollTop = 60;
            } else {
                mesagesListRef.current.scrollTop = mesagesListRef.current.scrollHeight;
            }
        }
    }

    render() {
        const {
            cbScroll, dataEntries, classes, idRoot,
        } = this.props;

        return (
            <div
                onScroll={cbScroll}
                ref={this.mesagesListRef}
                className={classes.listContainer}
            >
                {dataEntries.map(entry => (
                    <Fragment key={`key_${entry.id}`}>
                        <ListMessageStyled {...entry} idRoot={idRoot} />
                    </Fragment>
                ))}
            </div>
        );
    }
}

ChatMessagesList.propTypes = {
    cbScroll: PropTypes.func.isRequired,
    dataEntries: PropTypes.arrayOf(PropTypes.object).isRequired,
    keepScrollOnUpdate: PropTypes.bool.isRequired,
    classes: PropTypes.object.isRequired,
    idRoot: PropTypes.string.isRequired,
};

export default withStyles(styles)(ChatMessagesList);

/* eslint-enable max-classes-per-file */
