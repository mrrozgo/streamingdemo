/* eslint-disable max-classes-per-file */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { debounce } from 'lodash';
import { withStyles } from '@material-ui/styles';
import InputAdornment from '@material-ui/core/InputAdornment';

import {
    TextField, Button, Avatar, FormControl, Typography,
} from '@material-ui/core';

import { STREAM_STATE_SUCCESS } from '../../constants';
import * as awsApi from '../../api/aws';
import getLcString from '../../locale';

import MessagesList from './MessagesList';

import styles from './styles';

const ChatStatus = withStyles(styles)(({
    isMsgSending, areMsgsFetching, streamState,
    lastOpErr, dataEntries, classes,
}) => {
    if (dataEntries.length !== 0) {
        return null;
    }

    if (isMsgSending) {
        return (
            <div className={classes.statusFetching}>
                {getLcString('status_chat_sending')}
            </div>
        );
    }

    if (streamState !== STREAM_STATE_SUCCESS) {
        return (
            <div className={classes.statusDisconnect}>
                {getLcString('status_chat_not_connected')}
            </div>
        );
    }

    if (areMsgsFetching) {
        return (
            <div className={classes.statusFetching}>
                {getLcString('status_chat_fetching')}
            </div>
        );
    }

    if (lastOpErr) {
        return (
            <div className={classes.statusFetching}>
                {`Error: ${lastOpErr}`}
            </div>
        );
    }

    if (dataEntries.length === 0) {
        return (
            <div className={classes.statusDisconnect}>
                <Typography component='h1'>
                    {getLcString('status_chat_empty_header')}
                </Typography>
                <Typography component='p'>
                    {getLcString('status_chat_empty_content')}
                </Typography>
            </div>
        );
    }

    return null;
});

ChatStatus.propTypes = {
    isMsgSending: PropTypes.bool.isRequired,
    areMsgsFetching: PropTypes.bool.isRequired,
    lastOpErr: PropTypes.string.isRequired,
    dataEntries: PropTypes.arrayOf(PropTypes.object).isRequired,
    streamState: PropTypes.number.isRequired,
};

class StreamChat extends Component {
    constructor(props) {
        super(props);

        this.state = {
            allMsgsReceived: false,
            lastOpErr: '',
            token: '',
            limit: 8,
            chatMsgList: [],
            message: '',
            areMsgsFetching: false,
            isMsgSending: false,
            keepScrollOnUpdate: false,
        };

        this.msgSubscription = null;
        this.mesagesListRef = React.createRef();

        this.onSubmitMsg = this.onSubmitMsg.bind(this);
        this.onReceiveNewMsg = this.onReceiveNewMsg.bind(this);
        this.onScrollMsgList = this.onScrollMsgList.bind(this);
        this.onChangeMsg = this.onChangeMsg.bind(this);
        this.getOldMsgsChunk = this.getOldMsgsChunk.bind(this);
        this.handleScroll = debounce(this.handleScroll, 850);
    }

    componentDidMount() {
        const { streamroom, streamState } = this.props;

        if (streamState === STREAM_STATE_SUCCESS && streamroom && streamroom.length > 0) {
            this.msgSubscription = awsApi.subOnAddMsg(streamroom, this.onReceiveNewMsg);
            this.getOldMsgsChunk(false);
        }
    }

    componentDidUpdate(prevProps) {
        const { streamroom, userid, streamState } = this.props;

        if (prevProps.streamState !== streamState /* && prevProps.streamroom !== streamroom */) {
            if (this.msgSubscription) {
                this.msgSubscription.unsubscribe();
                this.msgSubscription = null;
            }

            this.setState({
                allMsgsReceived: false,
                chatMsgList: [],
                token: '',
            }, () => {
                if (streamState === STREAM_STATE_SUCCESS) {
                    this.msgSubscription = awsApi.subOnAddMsg(streamroom, this.onReceiveNewMsg);
                    this.getOldMsgsChunk(false);
                }
            });
        }
    }

    componentWillUnmount() {
        if (this.msgSubscription) {
            this.msgSubscription.unsubscribe();
            this.msgSubscription = null;
        }
    }

    onReceiveNewMsg(data) {
        const { state: { chatMsgList }, props: { userid }, mesagesListRef: { current } } = this;

        // perform scrolling to bottom only for the current user or if you are already in the bottom
        // this.messagesEnd.current.scrollIntoView({ behavior: 'smooth' })
        // const needScroll = data.userid === userid || (current.scrollHeight - current.scrollTop) === current.clientHeight;

        this.setState({
            chatMsgList: chatMsgList.concat(data),
            keepScrollOnUpdate: false,
        });

        // if (needScroll) {
        //     current.scrollTop = current.scrollHeight;
        // }
    }

    onChangeMsg(e) {
        e.preventDefault();

        this.setState({
            message: e.target.value,
        });
    }

    onSubmitMsg(e) {
        const { props: { streamroom, userid }, state: { message } } = this;

        e.preventDefault();

        if (this.isSubmitDisabled()) {
            return;
        }

        this.setState({
            isMsgSending: true,
        }, () => {
            awsApi.addChatMessage(streamroom, userid, message)
                .then(() => {
                    this.setState({
                        isMsgSending: false,
                        message: '',
                    });
                });
        });
    }

    onScrollMsgList(e) {
        e.persist();
        this.handleScroll(e);
    }

    // !!!optimization? empty?
    getOldMsgsChunk(keepScrolPos = false) {
        const {
            state: {
                limit, token, chatMsgList, allMsgsReceived, areMsgsFetching,
            },
            props: { streamroom },
        } = this;

        if (allMsgsReceived || areMsgsFetching) {
            return;
        }

        this.setState({
            areMsgsFetching: true,
        }, () => {
            awsApi.getChatMessages(streamroom, limit, token)
                .then((result) => {
                    // const __heightOld = this.mesagesListRef.current.scrollHeight;

                    this.setState({
                        chatMsgList: [...result.items.reverse(), ...chatMsgList],
                        areMsgsFetching: false,
                        token: result.nextToken,
                        allMsgsReceived: !result.nextToken,
                        keepScrollOnUpdate: keepScrolPos,
                    });
                });
        });
    }

    handleScroll(e) {
        if (e.target.scrollTop === 0) {
            requestAnimationFrame(() => this.getOldMsgsChunk(true));
        }
    }

    isSubmitDisabled() {
        const {
            state: { isMsgSending, areMsgsFetching, message },
            props: { streamroom, streamState },
        } = this;

        return isMsgSending || areMsgsFetching || streamState !== STREAM_STATE_SUCCESS
            || message.trim().length === 0 || streamroom.length === 0;
    }

    render() {
        const {
            state: {
                message, chatMsgList, isMsgSending, areMsgsFetching,
                lastOpErr, keepScrollOnUpdate,
            },
            props: {
                userid, classes, streamroom, streamState,
            },
        } = this;

        const submitDisabled = this.isSubmitDisabled();

        return (
            <div className={classes.streamChatContainer}>
                <div className={classes.listHeader}>
                    {streamState === STREAM_STATE_SUCCESS ? streamroom : '' }
                </div>
                <ChatStatus
                    isMsgSending={isMsgSending}
                    areMsgsFetching={areMsgsFetching}
                    lastOpErr={lastOpErr}
                    dataEntries={chatMsgList}
                    streamState={streamState}
                />
                <MessagesList
                    idRoot={userid}
                    cbScroll={this.onScrollMsgList}
                    dataEntries={chatMsgList}
                    keepScrollOnUpdate={keepScrollOnUpdate}
                />
                <form
                    onSubmit={this.onSubmitMsg}
                    className={classes.addMsgForm}
                >
                    <FormControl className={classes.addMsgFormControl}>
                        <TextField
                            variant='standard'
                            autoFocus={false}
                            label={`${getLcString('input_chatmsg_add_label')} ${userid}`}
                            fullWidth
                            className={classes.addMsgInput}
                            disabled={isMsgSending || areMsgsFetching}
                            value={message}
                            onChange={this.onChangeMsg}
                            InputProps={{
                                startAdornment: (
                                    <InputAdornment position='start'>
                                        <Avatar className={classes.addMsgAva} alt='ava'>
                                            {userid.length > 1 ? userid[0] + userid[1] : userid[0]}
                                        </Avatar>
                                    </InputAdornment>
                                ),
                            }}
                        />
                    </FormControl>
                    <FormControl>
                        <Button
                            onClick={this.onSubmitMsg}
                            size='small'
                            className='msg-submit'
                            disabled={submitDisabled}
                            variant='contained'
                            color='primary'
                        >
                            {isMsgSending
                                ? getLcString('btn_addmsg_sending')
                                : getLcString('btn_addmsg_default')}
                        </Button>
                    </FormControl>
                </form>
            </div>
        );
    }
}

StreamChat.propTypes = {
    streamroom: PropTypes.string.isRequired,
    userid: PropTypes.string.isRequired,
    classes: PropTypes.object.isRequired,
    streamState: PropTypes.number.isRequired,
};

export default withStyles(styles)(StreamChat);

/* eslint-enable max-classes-per-file */
